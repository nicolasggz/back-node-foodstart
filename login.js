import React,{Component} from "react";
import imagen1 from "../img/imagen2.jpg";
import axios from "axios";
import Cookies from "universal-cookie";

const baseUrl="http://localhost:3001/usuarios";
const cookies = new Cookies();

class Login extends Component {
  state={
    form:{
      username:"",
      password:""
    }
  }

  handleChange =async e=>{
    await this.setState({
      form:{
        ...this.state.form,
        [e.target.name]:e.target.value
      }
    });
  }

  iniciarSesion=async()=>{
    /* json-server --watch db.json --port 3001",    
    vital poner esta linea de codigo en package.json del script para correr el login(solo una vez, ejecutar y borrar la linea esto para abrir el server) */
    await axios.get(baseUrl,{params:{username:this.state.form.usuario, password:this.state.form.password}})
    .then(response=>{
      return response.data;
    })
    .then(response=>{
      if(response.length>0){
        var respuesta=response[0];
        cookies.set('id', respuesta.id, {path:"/"});
        console.log("bienvenido usuario");
        window.location.href="./menu";
      }else{
        console.log("usuario o contraseñas con incorrrectas"); 
      }
    })
    .catch(error=>{
      console.log(error);
    })
  }
  render(){
    return(
      <div className="todo_login">
        <div className="d-flex justify-content-center h-100">
          <div className="user_card">
            <div className="d-flex justify-content-center">
              <div className="brand_logo_container">
              <img src={imagen1} className="brand_logo" alt="Logo"/>
              </div>
            </div>
            <div className="d-flex justify-content-center form_container">
              <form>
                <div className="input-group mb-3">
                  <input type="text" name="username" className="form-control input_user"  placeholder="usuario" onChange={this.handleChange}/>
                </div>
                <div className="input-group mb-2">
                  <input type="password" name="password" className="form-control input_pass"  placeholder="contraseña" onChange={this.handleChange}/>
                </div>
                <div className="form-group">
                  <div className="custom-control custom-checkbox">
                    <input type="checkbox" className="custom-control-input" id="customControlInline"/>
                    <label className="custom-control-label">Recordar usuario</label>
                  </div>
                </div>
                <div className="d-flex justify-content-center mt-3 login_container">
                  <button type="submit" className="btn login_btn" onClick={()=>this.iniciarSesion()}>Iniciar Sesion</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Login;